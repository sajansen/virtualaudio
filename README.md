# VirtualAudio

_Visualisation software for audio systems_

![Example of highlighted path of a selected component](img/virtualaudio0.png)

### Setup

Create your own setup and add it to `src/js/setups/` or just import it on the webpage with the _Import_ button.

Take `defaultSetup.jsx` for example: first create the component with all it's properties. Next, connect it to another component. Finally, specify it's position on screen.

Available _ConnectionTypes_ are: Cable and Wireless.

Available _IOputTypes_ are: XLR, Jack, Minijack, Speakon, Tulp, and Wireless.

Component images are found in `img/components/`. You can add more images to this folder. 

### Usage

You can drag any component.

Click a component to highlight the route it's signal will be following. This way, you can track the signal of this component, and signals flowing through this component.

When you've selected a component, hold the Ctrl-key and select another component you want to connect to. The Connection Form will be prepared with these components.

Double click a component to see the internal connections and more information about the component. In the internal connections, you can route the component's input to outputs. 

Finally, you can export your scheme by copying the output of the _Export_ button or saving the export to a file. 

### Develop

Run `npm install` and `npm run watch` to begin working on the code. 


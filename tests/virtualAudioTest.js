import {Component, Scheme} from "~/js/virtualaudio";

const assert = require('chai').assert;
const expect = require('chai').expect;

describe("VirtualAudio", function () {
  describe("Scheme", function () {
    let scheme = new Scheme();
    beforeEach(function() {
      scheme = new Scheme();
    });

    it("should give unique new ID's", function (){
      const component1 = new Component({});
      component1.id = 1;
      const component4 = new Component({});
      component4.id = 4;
      const component2 = new Component({});
      component2.id = 2;

      scheme.addComponent(component1);
      scheme.addComponent(component4);
      scheme.addComponent(component2);

      assert.equal(scheme.components.length, 3);
      assert.equal(scheme.getNewComponentID(), 5);
    });

    it("should add a new component", function (){
      const component1 = new Component({});
      component1.id = 1;

      scheme.addComponent(component1);
      assert.equal(scheme.components.length, 1);
    });
    it("should add a new component without a ID and give it a new ID", function (){
      const component1 = new Component({});
      component1.id = null;

      scheme.addComponent(component1);

      assert.equal(scheme.components.length, 1);
      assert.notEqual(component1.id, null);
    });
    it("should throw an error when new component's ID is not unique", function (){
      const component1 = new Component({name: "Good component"});
      component1.id = 1;
      const component2 = new Component({name: "Bad component"});
      component2.id = 1;

      scheme.addComponent(component1);
      expect(() => scheme.addComponent(component2)).to.throw('Component Bad component already exists in sheme\'s components!');

      assert.equal(scheme.components.length, 1);
    });

    it("should give the right component", function (){
      const component1 = new Component({});
      const component2 = new Component({});
      const component3 = new Component({});

      scheme.addComponent(component1);
      scheme.addComponent(component2);
      scheme.addComponent(component3);

      assert.equal(scheme.getComponent(component2.id), component2);
    });
    it("should give null when asked for a non existing component", function (){
      const component1 = new Component({});

      scheme.addComponent(component1);

      assert.equal(scheme.getComponent(100), null);
    });
  });
});


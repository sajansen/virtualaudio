// Load style sheets
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css'
import './style/visualiser.sass';

// Load other stuff
require('bootstrap');
require("jquery-ui-bundle");
import './js/utils.js';

// Load / initiate VirtualAudio
import './js/virtualaudio.js';

// Start the application
import './js/main.jsx';

// Some other stuff
import './js/theme';


import {
  Amplifier,
  Component,
  connect, ConnectionType,
  DI,
  Geometry,
  IOput,
  IOputType,
  Microphone,
  Mixer,
  Snake,
  Speaker, WirelessReceiver
} from "~/js/virtualaudio";
import {connectAllInputsToOutput} from "~/js/utils";

function MicrophoneFluit({name}, scheme = null) {
  const component = Microphone({name: name}, scheme);
  component.image = "sd_systems_fx_0";
  component.outputs[0].type = IOputType.JACK;
  return component;
}

function createMixer24ch(scheme) {
  const mixer24ch = Mixer({name: "Mixer", inputChannels: 24, outputChannels: 14}, scheme);
  mixer24ch.outputs[0].setName("aux1").setType(IOputType.XLR);
  mixer24ch.outputs[1].setName("aux2").setType(IOputType.XLR);
  mixer24ch.outputs[2].setName("aux3").setType(IOputType.XLR);
  mixer24ch.outputs[3].setName("aux4").setType(IOputType.XLR);
  mixer24ch.outputs[4].setName("aux5").setType(IOputType.JACK);
  mixer24ch.outputs[5].setName("aux6").setType(IOputType.JACK);
  mixer24ch.outputs[6].setName("mtxL").setType(IOputType.XLR);
  mixer24ch.outputs[7].setName("mtxR").setType(IOputType.XLR);
  mixer24ch.outputs[8].setName("grp1").setType(IOputType.XLR);
  mixer24ch.outputs[9].setName("grp2").setType(IOputType.XLR);
  mixer24ch.outputs[10].setName("grp3").setType(IOputType.XLR);
  mixer24ch.outputs[11].setName("grp4").setType(IOputType.XLR);
  mixer24ch.outputs[12].setName("mainL").setType(IOputType.XLR);
  mixer24ch.outputs[13].setName("mainR").setType(IOputType.XLR);

  mixer24ch.connect("1").to(["grp1", "grp2", "mainL", "mainR"]);
  mixer24ch.connect("2").to(["grp1", "grp2", "mainL", "mainR"]);
  mixer24ch.connect("3").to(["grp1", "grp2", "mainL", "mainR"]);
  mixer24ch.connect("4").to(["grp1", "grp2", "mainL", "mainR"]);
  mixer24ch.connect("5").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("6").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("7").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("8").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("9").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("10").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("11").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("12").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("13").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("14").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("15").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("16").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("17").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("18").to(["grp3", "grp4", "mainL", "mainR"]);
  mixer24ch.connect("19").to(["mainL", "mainR"]);
  mixer24ch.connect("20").to(["mainL", "mainR"]);
  mixer24ch.connect("21").to(["mainL", "mainR"]);
  mixer24ch.connect("22").to(["mainL", "mainR"]);
  mixer24ch.connect("23").to(["aux6"]);
  mixer24ch.connect("24").to(["aux6"]);

  return mixer24ch;
}

function createMixerUitzending(scheme) {
  const mixerUitzending = Mixer({name: "Uitzending", inputChannels: 12, outputChannels: 2}, scheme);
  mixerUitzending.inputs[5].setType(IOputType.JACK);
  mixerUitzending.inputs[6].setType(IOputType.JACK);
  mixerUitzending.inputs[7].setType(IOputType.JACK);
  mixerUitzending.inputs[8].setType(IOputType.JACK);
  mixerUitzending.inputs[9].setType(IOputType.JACK);
  mixerUitzending.inputs[10].setType(IOputType.JACK);
  mixerUitzending.inputs[11].setType(IOputType.JACK);

  mixerUitzending.outputs[0].setName("mainL").setType(IOputType.JACK);
  mixerUitzending.outputs[1].setName("mainR").setType(IOputType.JACK);

  connectAllInputsToOutput(mixerUitzending, "mainL");
  connectAllInputsToOutput(mixerUitzending, "mainR");

  return mixerUitzending;
}

export default function setupForRehobothkerk(scheme) {
  scheme.reset();
  scheme.name = "Rehobothkerk Woerden";
  scheme.version = "1.0.0";

  const micPianoL = Microphone({name: "Piano L"}, scheme);
  const micPianoR = Microphone({name: "Piano R"}, scheme);
  const micFluit = MicrophoneFluit({name: "Fluit"}, scheme);
  const micGitaar = Microphone({name: "Gitaar"}, scheme);
  const micZaalL = Microphone({name: "Zaal L"}, scheme);
  const micZaalR = Microphone({name: "Zaal R"}, scheme);

  const diFLuit = DI({name: "Fluit"}, scheme);
  const diGitaar = DI({name: "Gitaar"}, scheme);

  const snake = Snake({name: "Snake podium"}, scheme);

  const micHand = Microphone({name: "Handheld"}, scheme);
  micHand.image = "sennheiser_ew322_g3_mic_0";
  micHand.outputs[0].type = IOputType.WIRELESS;
  const micHeadset = Microphone({name: "Headset"}, scheme);
  micHeadset.image = "sennheiser_ew122_beltpack_0";
  micHeadset.outputs[0].type = IOputType.WIRELESS;
  const receiverHand = WirelessReceiver({name: "Handheld receiver"}, scheme);
  const receiverHeadset = WirelessReceiver({name: "Headset receiver"}, scheme);

  const mixer24ch = createMixer24ch(scheme);

  const amplifierZaal = Amplifier({name: "Zaal"}, scheme);
  const amplifierBijzaal = Amplifier({name: "Bijzaal"}, scheme);
  const amplifierMonitoren = Amplifier({name: "Monitoren"}, scheme);

  const speakerZaalL = Speaker({name: "Zaal L"}, scheme);
  const speakerZaalR = Speaker({name: "Zaal R"}, scheme);
  const speakerBijzaalL = Speaker({name: "Bijzaal L"}, scheme);
  const speakerBijzaalR = Speaker({name: "Bijzaal R"}, scheme);

  const mixerUitzending = createMixerUitzending(scheme);

  /** Define connections **/

  connect(micFluit).to(diFLuit, "1");

  connect(micPianoL).to(snake, "7");
  connect(micPianoR).to(snake, "8");
  connect(diGitaar, "1").to(snake, "9");
  connect(micGitaar).to(snake, "10");
  connect(diFLuit, "1").to(snake, "11");
  connect(diFLuit, "2").to(snake, "12");
  connect(micZaalL).to(snake, "23");
  connect(micZaalR).to(snake, "24");

  connect(snake, "1").to(mixer24ch, "1");
  connect(snake, "2").to(mixer24ch, "2");
  connect(snake, "3").to(mixer24ch, "3");
  connect(snake, "4").to(mixer24ch, "4");
  connect(snake, "5").to(mixer24ch, "5");
  connect(snake, "6").to(mixer24ch, "6");
  connect(snake, "7").to(mixer24ch, "7");
  connect(snake, "8").to(mixer24ch, "8");
  connect(snake, "9").to(mixer24ch, "9");
  connect(snake, "10").to(mixer24ch, "10");
  connect(snake, "11").to(mixer24ch, "11");
  connect(snake, "12").to(mixer24ch, "12");
  connect(snake, "13").to(mixer24ch, "13");
  connect(snake, "14").to(mixer24ch, "14");
  connect(snake, "15").to(mixer24ch, "15");
  connect(snake, "16").to(mixer24ch, "16");
  connect(snake, "17").to(mixer24ch, "17");
  connect(snake, "18").to(mixer24ch, "18");
  connect(snake, "19").to(mixer24ch, "19");
  connect(snake, "20").to(mixer24ch, "20");
  // connect(snake, "21").to(mixer24ch, "21");
  // connect(snake, "22").to(mixer24ch, "22");
  connect(snake, "23").to(mixer24ch, "23");
  connect(snake, "24").to(mixer24ch, "24");

  connect(micHand).with(ConnectionType.WIRELESS).to(receiverHand);
  connect(micHeadset).with(ConnectionType.WIRELESS).to(receiverHeadset);
  connect(receiverHand, "line").to(mixer24ch, "22");
  connect(receiverHeadset, "line").to(mixer24ch, "21");

  connect(mixer24ch, "mainL").to(amplifierZaal, "1");
  connect(mixer24ch, "mainR").to(amplifierZaal, "2");
  connect(mixer24ch, "mtxL").to(amplifierBijzaal, "1");
  connect(mixer24ch, "mtxR").to(amplifierBijzaal, "2");
  connect(mixer24ch, "aux2").to(amplifierMonitoren, "1");
  connect(mixer24ch, "aux3").to(amplifierMonitoren, "2");
  connect(mixer24ch, "aux6").to(mixerUitzending, "1");
  connect(mixer24ch, "grp1").to(mixerUitzending, "5");
  connect(mixer24ch, "grp2").to(mixerUitzending, "6");
  connect(mixer24ch, "grp3").to(mixerUitzending, "9");
  connect(mixer24ch, "grp4").to(mixerUitzending, "10");

  connect(amplifierZaal, "1").to(speakerZaalL);
  connect(amplifierZaal, "2").to(speakerZaalR);
  connect(amplifierBijzaal, "1").to(speakerBijzaalL);
  connect(amplifierBijzaal, "2").to(speakerBijzaalR);



  /** Define positions **/

  diGitaar.geometry = new Geometry(10, 55);
  micGitaar.geometry.placeRightOf(diGitaar);
  micPianoL.geometry.placeRightOf(micGitaar);
  micPianoR.geometry.placeRightOf(micPianoL);
  micFluit.geometry.placeRightOf(micPianoR);
  micZaalL.geometry.placeRightOf(micFluit).moveHalfLengthRight();
  micZaalR.geometry.placeRightOf(micZaalL);
  diFLuit.geometry.placeUnderneathOf(micFluit);

  snake.geometry.placeUnderneathOf(diFLuit);
  micHeadset.geometry.placeLeftOf(snake).moveOneLengthLeft();
  micHand.geometry.placeLeftOf(micHeadset);
  receiverHeadset.geometry.placeUnderneathOf(micHeadset);
  receiverHand.geometry.placeUnderneathOf(micHand);

  mixer24ch.geometry.placeUnderneathOf(snake).moveHalfLengthDown();
  mixerUitzending.geometry.placeRightOf(mixer24ch).moveOneLengthRight();

  amplifierBijzaal.geometry.placeUnderneathOf(mixer24ch).moveHalfLengthDown();
  amplifierZaal.geometry.placeRightOf(amplifierBijzaal).moveOneLengthRight();
  amplifierMonitoren.geometry.placeLeftOf(amplifierBijzaal);

  speakerZaalL.geometry.placeUnderneathOf(amplifierZaal).moveHalfLengthLeft();
  speakerZaalR.geometry.placeRightOf(speakerZaalL);
  speakerBijzaalL.geometry.placeUnderneathOf(amplifierBijzaal).moveHalfLengthLeft();
  speakerBijzaalR.geometry.placeRightOf(speakerBijzaalL);
}
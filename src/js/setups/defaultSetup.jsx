import {
  Amplifier,
  connect,
  ConnectionType,
  DI,
  Geometry,
  IOputType,
  Microphone,
  Mixer,
  Snake,
  Speaker
} from "~/js/virtualaudio";

export default function setupForDefault(scheme) {
  scheme.reset();
  scheme.name = "Default scheme";
  scheme.version = "1.0.1";

  const mic1 = Microphone({name: "Mic"}, scheme);
  const mic2 = Microphone({name: "Mic 2"}, scheme);
  const mic3 = Microphone({name: "Mic 3"}, scheme);

  const mic4 = Microphone({name: "Mic fluit"}, scheme);
  mic4.image = "sd_systems_fx_0";
  mic4.outputs[0].type = IOputType.JACK;

  const di1 = DI({name: "DI fluit"}, scheme);

  const snake = Snake({}, scheme);

  const mixer1 = Mixer({name: "Mixer", inputChannels: 4}, scheme);
  mixer1.connect("1").to("mainL");
  mixer1.connect("2").to("mainR");
  mixer1.connect("3").to("mainL");
  mixer1.connect("3").to("mainR");
  mixer1.connect("4").to("mainL");

  const mixer2 = Mixer({
                         name: "QU-24",
                         inputChannels: 24,
                         outputChannels: 12,
                         image: "qu24_0",
                       }, scheme);

  const amp1 = Amplifier({name: "Zaal amp"}, scheme);

  const speaker1 = Speaker({name: "Zaal L"}, scheme);
  const speaker2 = Speaker({name: "Zaal R"}, scheme);

  /** Define connections **/

  connect(mic1).to(mixer1, "1");
  connect(mic2, "0").with(ConnectionType.CABLE).to(snake, "2");
  connect(mic3).to(snake, "3");

  connect(mic4).to(di1, "1");
  connect(di1, "1").to(snake, "4");

  connect(snake, "2").to(mixer1, "2");
  connect(snake, "3").to(mixer1, "3");
  connect(snake, "4").to(mixer1, "4");

  connect(mixer1, "mainL").to(amp1, "1");
  connect(mixer1, "mainR").to(amp1, "2");

  connect(amp1, "1").to(speaker1);
  connect(amp1, "2").to(speaker2);


  /** Define positions **/

  mic1.geometry = new Geometry(50, 50);
  mic2.geometry.placeRightOf(mic1);
  mic3.geometry.placeRightOf(mic2);
  mic4.geometry.placeRightOf(mic3);
  di1.geometry.placeUnderneathOf(mic4);

  snake.geometry.placeUnderneathOf(di1).x = 200;
  mixer1.geometry.placeUnderneathOf(snake);
  mixer2.geometry.placeRightOf(mixer1);

  amp1.geometry.placeUnderneathOf(mixer1);
  speaker1.geometry.placeUnderneathOf(amp1).x = 120;
  speaker2.geometry.placeRightOf(speaker1);
}
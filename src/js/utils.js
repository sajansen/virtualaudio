/**
 * Format a string according to:
 *  "Hello, {name}, are you feeling {adjective}?".sprintf({name:"Gabriel", adjective: "OK"});
 * @link https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format/4673436#4673436
 * @type {Function}
 */
String.prototype.sprintf = String.prototype.sprintf ||
    function () {
        "use strict";
        var str = this.toString();
        if (arguments.length) {
            var t = typeof arguments[0];
            var key;
            var args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };

Array.prototype.equals = function (array){
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    return $(this).not(array).length === 0 && $(array).not(this).length === 0
};

export function connectAllInputsToOutput(component, output) {
  component.inputs.forEach(input => {
    component.connect(input.name.toString()).to(output);
  });
}
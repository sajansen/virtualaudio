import * as tools from './visualiser-tools.js';
import * as build from './visualiser-build.js';
import * as canvas from './visualiser-paint.js';
import * as form from './visualiser-form.jsx';
import * as global from './visualiser-globals.js';
import * as modal from './visualiser-modal.jsx';
import * as va from './virtualaudio.js';
import {debug} from './debugging.js';
import {ConnectionType, Geometry, scheme} from "./virtualaudio";
import {Component} from "~/js/virtualaudio";
import setupForRehobothkerk from "~/js/setups/rehobothkerkSetup";

let ctrlIsPressed = false;

function addComponentDraggableEvent() {
  $(".component").draggable({
    start: function(e, ui) {
      componentOnClick(this);
      global.page.startAnimation();
    }, stop: function () {
      global.page.stopAnimation();
      build.updateSchemeComponent(null, this);
      canvas.repaint();
    }
  }).disableSelection();
}

function componentOnClick(self = null) {
    self = this || self;
    const componentID = parseInt($(self).attr("data-component-id"));
    const component = va.scheme.getComponent(componentID);

    if (ctrlIsPressed && global.page.selectedComponent) {
      if (global.page.selectedComponent !== component) {
        console.log("Ctrl mapping");
        form.fillConnectionFormOutputWith(global.page.selectedComponent);
        form.fillConnectionFormInputWith(component);
        return;
      }
    }

    // Highlight
    tools.highlightComponentRoute(component);
    canvas.repaint();
    build.buildHighlightRoute();

    // Form
    form.clearConnectionForm();
    form.fillComponentFormWith(component);
    form.fillConnectionFormWith(component);
  }

function addEvents() {
  addComponentDraggableEvent();

  $(document).on("click", "#visualisation", function (e) {
    // Prevent highlighting when clicking on components
    if (e.target !== this)
      return;

    tools.highlightComponentRoute();
    canvas.repaint();
    build.buildHighlightRoute();
  });

  $(document).on("click", ".component", componentOnClick);

  $(document).on('dblclick', '.component', function () {
    const componentID = parseInt($(this).attr("data-component-id"));
    const component = va.scheme.getComponent(componentID);
    modal.showComponent(component);
  });

  $(document).on("keydown", function (e) {
    const key = e.keyCode || e.charCode;
    if (key === 17) {
      ctrlIsPressed = true;
    }
  });

  $(document).on("keyup", function (e) {
    ctrlIsPressed = false;
  });

  $(document).on("keydown", "#visualisation-wrapper", function (e) {
    const key = e.keyCode || e.charCode;
    if (key !== 46)
      return;

    if (global.page.selectedComponent) {
      va.scheme.deleteComponent(global.page.selectedComponent.id);
      global.page.resetHighlightRoute();
      onComponentUpdate();
    }
  });
}

$(document).ready(() => {
  /**
   * Loading
   */
  setupForRehobothkerk(scheme);

  tools.fillComponentTemplatesList();

  /**
   * Building
   */
  build.createSchemeInfo();
  build.rebuild();


  /**
   * Painting
   */
  global.canvas.width = global.visualisation.getBoundingClientRect().width;
  global.canvas.height = global.visualisation.getBoundingClientRect().height;
  canvas.repaint();


  /**
   * Forms
   */
  form.fillComponentTemplatesList();
  form.fillComponentGroupsList();
  form.fillComponentTypesList();
  form.clearConnectionForm();

  // Prevent all forms from reloading page
  $("form").submit((e) => {
    e.preventDefault();
  });

  $("#component-new-form").submit(function () {
    const name = document.getElementById("componentNewName").value;
    const type = document.getElementById("componentNewType").value;
    const group = document.getElementById("componentNewGroup").value;
    const category = document.getElementById("componentNewCategory").value;
    const image = document.getElementById("componentNewImage").value;
    const inputs = document.getElementById("componentNewInputs").value;
    const outputs = document.getElementById("componentNewOutputs").value;
    $(this).trigger('reset');

    const component = new va.Component({
      name: name,
      type: type,
      group: group,
      category: category,
      image: image,
    })
      .addToScheme(va.scheme);
    // component.inputs = inputs;   // todo
    if (inputs.length > 0) {
      component.inputs = JSON.parse(inputs);
    }
    // component.outputs = outputs; // todo
    if (outputs.length > 0) {
      component.outputs = JSON.parse(outputs);
    }

    onComponentUpdate();
  });

  $("#connectionOutputComponent").change(function () {
    const selectedID = parseInt($(this).val());
    const component = (selectedID !== -1) ? va.scheme.getComponent(selectedID) : null;
    form.fillConnectionIOChannel(component, "outputs");
  });
  $("#connectionInputComponent").change(function () {
    const selectedID = parseInt($(this).val());
    const component = (selectedID !== -1) ? va.scheme.getComponent(selectedID) : null;
    form.fillConnectionIOChannel(component, "inputs");
  });

  $("#connection-new-form").submit(function (e) {
    const submitName = e.originalEvent.explicitOriginalTarget.name;     // With which submit button form was submitted

    const outputComponentID = parseInt(document.getElementById("connectionOutputComponent").value);
    const outputIOChannel = document.getElementById("connectionOutputIOChannel").value.toString();
    const inputComponentID = parseInt(document.getElementById("connectionInputComponent").value);
    const inputIOChannel = document.getElementById("connectionInputIOChannel").value.toString();
    const type = document.getElementById("connectionType").value;

    const noEmptyOutputFields = (outputComponentID !== -1 && outputIOChannel !== "-1");
    const allEmptyInputFields = (inputComponentID === -1 && inputIOChannel === "-1");
    const oneEmptyInputField = ((inputComponentID === -1 || inputIOChannel === "-1")
      && !allEmptyInputFields);

    if (!noEmptyOutputFields || oneEmptyInputField) {
      return
    }

    const outputComponent = va.scheme.getComponent(outputComponentID);

    // remove existing connection
    va.scheme.connections = va.scheme.connections.filter(
      c => !(c.output.component.id === outputComponent.id && c.output.channel === outputIOChannel)
    );

    if (!allEmptyInputFields) {
      const inputComponent = va.scheme.getComponent(inputComponentID);

      if (outputComponent === inputComponent) {
        return;
      }

      if (va.scheme.findConnectionWithInput(inputComponent, inputIOChannel)) {
        if (submitName === "force-add") {
          // Delete connection
          va.scheme.connections = va.scheme.connections.filter(
            c => !(c.input.component.id === inputComponent.id && c.input.channel === inputIOChannel)
          );
        } else {
          // We can't connect to a input which is already in use...
          return;
        }
      }

      va.connect(outputComponent, outputIOChannel)
        .with(type ? type : ConnectionType.CABLE)
        .to(inputComponent, inputIOChannel);
    }

    $(this).trigger('reset');
    tools.highlightComponentRoute(global.page.selectedComponent);
    build.buildHighlightRoute();
    canvas.repaint();
    form.clearConnectionForm();
  });

  $(document).on('click', '.component-template', (event) => {
    debug['c'] = event.currentTarget;
    const template_name = event.currentTarget.getAttribute('template-name');
    const component = tools.component_templates[template_name];
    const component_name = component.name + ' ' + (va.scheme.components.length + 1);
    console.log(component_name);

    // Create component
    const new_component = Object.assign({}, component);
    new_component.name = component_name;
    new_component.addToScheme(va.scheme);

    onComponentUpdate();
  });


  /**
   * Interaction
   */

  addEvents();

  $("#toolsImportButton").click(function () {
    const importText = document.getElementById("toolsTextarea").value;
    const json = JSON.parse(importText);

    const schemeBackup = Object.assign({}, va.scheme);

    function initComponentsFrom(components) {
      const list = [];
      components.forEach(component => {
        const g = new Geometry();
        g.x = component.geometry.x;
        g.y = component.geometry.y;
        g.width = component.geometry.width;
        g.height = component.geometry.height;
        g.zIndex = component.geometry.zIndex;

        const c = new Component({});
        c.id = component.id;
        c.name = component.name;
        c.type = component.type;
        c.group = component.group;
        c.category = component.category;
        c.image = component.image;
        c.inputs = component.inputs;
        c.outputs = component.outputs;
        c.internalConnections = component.internalConnections;

        c.geometry = g;

        list.push(c);
      });
      return list;
    }

    try {
      va.scheme.name = json.name;
      va.scheme.version = json.version;
      va.scheme.components = initComponentsFrom(json.components);
      va.scheme.connections = json.connections;

      document.getElementById("toolsTextarea").value = "Imported!";
    } catch (error) {
      console.error(error);
      document.getElementById("toolsTextarea").value = "Error! -> " + error;

      va.scheme.name = schemeBackup.name;
      va.scheme.version = schemeBackup.version;
      va.scheme.components = initComponentsFrom(schemeBackup.components);
      va.scheme.connections = schemeBackup.connections;
    }

    build.createSchemeInfo();
    onComponentUpdate();
  });

  $("#toolsExportButton").click(function () {
    updateToolsExport();
  });

  $("#toolsExportToFileButton").click(function () {
    updateToolsExport();
    const data = document.getElementById("toolsTextarea").value;
    const filename = "virtualaudio.json";
    const type = "application/json";

    const file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
      window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
      let a = document.createElement("a");
      let url = URL.createObjectURL(file);
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  });

  $("#overview-recalculate").click(function () {
    function createHeading(title) {
      const element = document.createElement("strong");
      element.innerText = title;
      return element;
    }
    function createListItem(title) {
      const element = document.createElement("li");
      element.innerText = title;
      return element;
    }
    const outputDiv = document.getElementById("overview-list");
    tools.clearChildren(outputDiv);

    outputDiv.appendChild(createHeading("Component"));
    const componentList = document.createElement("li");
    outputDiv.appendChild(componentList);
    va.scheme.components.forEach(component => {
      componentList.appendChild(createListItem(component.type));
    });
  })

});

function onComponentUpdate() {
  build.rebuild();
  form.fillComponentGroupsList();
  form.fillComponentTypesList();
  form.clearConnectionForm();
  canvas.repaint();
  addEvents();
}

function updateToolsExport() {
  document.getElementById("toolsTextarea").value = JSON.stringify({
    name: va.scheme.name,
    version: va.scheme.version,
    components: va.scheme.components,
    connections: va.scheme.connections,
  });
}


import * as global from './visualiser-globals.js';
import * as tools from './visualiser-tools.js';
import * as va from './virtualaudio.js';
import {debug} from './debugging.js';
import '~/style/visualisation-control.sass';

export function fillComponentTemplatesList() {
    let parentElement = document.getElementById("component-templates");
    tools.clearChildren(parentElement);

    for (const [template_name, component_template] of Object.entries(tools.component_templates)){
        parentElement.appendChild(createComponentTemplateDiv(template_name, component_template));
    }
}

function createComponentTemplateDiv(template_name, component) {
    const element = document.createElement("div");
    debug.element = element;
    element.classList.add('component-template');
    element.setAttribute('template-name', template_name);
    element.title = JSON.stringify(component);

    if (component.image){
        element.style.backgroundImage = "url('" + global.IMAGE_COMPONENT_PATH + "/" + component.image + global.IMAGE_COMPONENT_POSTFIX + "')";
    } else {
        element.style.backgroundColor = global.IMAGE_ABSENT_COLOR;
    }

    return element;
}

export function fillComponentGroupsList() {
    let listElement = document.getElementById("componentGroupsList");

    let currentList = [];
    Array.from(listElement.options).forEach((option) => {
        currentList.push(option.value);
    });

    let list = [];
    // Get all groups
    va.scheme.components.forEach((component) => {
        if (component.group && currentList.indexOf(component.group) < 0) {
            list.push(component.group);
        }
    });

    // Make array unique
    list = [...new Set(list)];

    list.forEach((value) => {
        let element = document.createElement("option");
        element.value = value;
        listElement.appendChild(element);
    })
}

export function fillComponentTypesList() {
    let listElement = document.getElementById("componentTypesList");

    let currentList = [];
    Array.from(listElement.options).forEach((option) => {
        currentList.push(option.value);
    });

    let list = [];
    // Get all types
    va.scheme.components.forEach((component) => {
        if (component.type.length > 0 && currentList.indexOf(component.type) < 0) {
            list.push(component.type);
        }
    });

    // Make array unique
    list = [...new Set(list)];

    // Add the new items which where not in the list yet
    list.forEach((value) => {
        let element = document.createElement("option");
        element.value = value;
        listElement.appendChild(element);
    })
}

export function fillComponentFormWith(component) {
    document.getElementById("componentNewName").value = component.name;
    document.getElementById("componentNewType").value = component.type;
    document.getElementById("componentNewGroup").value = component.group;
    document.getElementById("componentNewCategory").value = component.category;
    document.getElementById("componentNewImage").value = component.image;
    document.getElementById("componentNewInputs").value = JSON.stringify(component.inputs);
    document.getElementById("componentNewOutputs").value = JSON.stringify(component.outputs);
}

/**
 *  Connections
 */

export function fillConnectionComponents() {
    const connectionOutputComponent = document.getElementById("connectionOutputComponent");
    const connectionInputComponent = document.getElementById("connectionInputComponent");

    tools.clearChildren(connectionOutputComponent, true);
    tools.clearChildren(connectionInputComponent, true);

    va.scheme.components.forEach((component) => {
        let element = document.createElement("option");
        element.value = component.id;
        element.text = "{name}".sprintf(component);
        if (component.outputs.length > 0) {
            connectionOutputComponent.appendChild(element);
        }
        if (component.inputs.length > 0) {
            connectionInputComponent.appendChild(element.cloneNode(true));
        }
    });

    if (connectionOutputComponent.children.length === 2){
        connectionOutputComponent.children[1].selected = true;
    }
    if (connectionInputComponent.children.length === 2){
        connectionInputComponent.children[1].selected = true;
    }
}

export function fillConnectionIOChannel(component, iomode) {
    let connectionChannel;
    let iomodeSingle, invertIOMode;
    if (iomode === "outputs"){
        connectionChannel = document.getElementById("connectionOutputIOChannel");
        iomodeSingle = "output";
        invertIOMode = "input";
    }else if (iomode === "inputs") {
        connectionChannel = document.getElementById("connectionInputIOChannel");
        iomodeSingle = "input";
        invertIOMode = "output";
    }else{
        throw "Invalid IOmode given!";
    }

    tools.clearChildren(connectionChannel, true);

    if (component) {
        va.scheme.getComponent(component.id)[iomode].forEach((ioput) => {
            const element = document.createElement("option");
            element.value = ioput.name;
            element.innerHTML = "{name}: {type}".sprintf(ioput);

            const connectedTo = va.scheme.connections.find(function (c) {
                return c[iomodeSingle].component.id === component.id && c[iomodeSingle].channel === ioput.name;
            });
            if (connectedTo){
                const componentFound = connectedTo[invertIOMode].component;
                element.innerHTML += " &nbsp; {direction} &nbsp; {channel}: {component}".sprintf({
                    direction: invertIOMode === "input" ? "->" : "<-",
                    channel: connectedTo[invertIOMode].channel,
                    component: componentFound.name
                });

                if (global.page.selectedComponent && global.page.selectedComponent.id === componentFound.id) {
                    element.selected = true;
                }
            }

            // Check if this is the only io, if so, select it by default
            if (component[iomode].length === 1) {
                element.selected = true;
            }

            connectionChannel.appendChild(element);
        });
    }

    // Set display size of list
    connectionChannel.size = Math.min(connectionChannel.children.length, 16);
}

export function clearConnectionForm() {
    fillConnectionComponents();
    const outputComponentID = parseInt(document.getElementById("connectionOutputComponent").value);
    const inputComponentID = parseInt(document.getElementById("connectionInputComponent").value);

    let outputComponent = null;
    if (outputComponentID !== -1){
        outputComponent = va.scheme.getComponent(outputComponentID);
    }
    let inputComponent = null;
    if (inputComponentID !== -1){
        inputComponent = va.scheme.getComponent(inputComponentID);
    }

    fillConnectionIOChannel(outputComponent, "outputs");
    fillConnectionIOChannel(inputComponent, "inputs");
}

export function fillConnectionFormWith(component) {
    fillConnectionFormOutputWith(component);
    fillConnectionFormInputWith(component);
}


export function fillConnectionFormOutputWith(component) {
    const connectionOutputComponent = document.getElementById("connectionOutputComponent");
    if (component.outputs.length > 0) {
        connectionOutputComponent.value = component.id;
        fillConnectionIOChannel(component, "outputs");
    }
}

export function fillConnectionFormInputWith(component) {
    const connectionInputComponent = document.getElementById("connectionInputComponent");
    if (component.inputs.length > 0) {
        connectionInputComponent.value = component.id;
        fillConnectionIOChannel(component, "inputs");
    }
}
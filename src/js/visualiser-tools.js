import * as global from './visualiser-globals.js';
import * as va from './virtualaudio.js';

export function highlightComponentRoute(component) {
    global.page.resetHighlightRoute();

    if (!component)
        return;

    global.page.selectedComponent = component;
    global.page.selectedComponent.geometry.zIndex = 1;

    function getUpstream(component, channel){
        const connection = va.scheme.findConnectionWithInput(component, channel);
        if (!connection){
            // We're done
            return;
        }
        let outputComponent = va.scheme.getComponent(connection.output.component.id);

        const connectionInFoundList = global.page.highlightRoute.upstream.connections.some(c => c.input == connection.output);
        if (connectionInFoundList){
            // Prevent loops
            return;
        }

        connection.highlighted = true;
        global.page.highlightRoute.upstream.connections.push(connection);

        if (!outputComponent)
            return;

        outputComponent.highlighted = true;
        global.page.highlightRoute.upstream.components.push(outputComponent);

        outputComponent.findInternalConnectionsWithOutput(connection.output.channel).forEach(function(internalConnection){
            getUpstream(outputComponent, internalConnection.inputChannel);
        });
    }

    component.inputs.forEach(function(input){
        getUpstream(component, input.name);
    });

    function getDownstream(component, channel){
        const connection = va.scheme.findConnectionWithOutput(component, channel);
        if (!connection){
            // We're done
            return;
        }
        let inputComponent = va.scheme.getComponent(connection.input.component.id);

        const connectionInFoundList = global.page.highlightRoute.upstream.connections.some(c => c.input == connection.output);
        if (connectionInFoundList){
            // Prevent loops
            return;
        }

        connection.highlighted = true;
        global.page.highlightRoute.downstream.connections.push(connection);

        if (!inputComponent)
            return;

        inputComponent.highlighted = true;
        global.page.highlightRoute.downstream.components.push(inputComponent);
        
        inputComponent.findInternalConnectionsWithInput(connection.input.channel).forEach(function(internalConnection){
            getDownstream(inputComponent, internalConnection.outputChannel);
        });
    }
    component.outputs.forEach(function(output){
        getDownstream(component, output.name);
    });
}

export function clearChildren(element, preserveFirst=false) {
    while (element.lastChild){
        if (preserveFirst && element.children.length === 1){
            break;
        }
        element.removeChild(element.lastChild);
    }
}

export const component_templates = {};

export function fillComponentTemplatesList() {
    component_templates['Default microphone'] = va.Microphone({name: 'Microphone'});
    component_templates['Default DI'] = va.DI({name: 'DI'});
    component_templates['Default Wireless Receiver'] = va.WirelessReceiver({name: 'Receiver'});
    component_templates['Default snake'] = va.Snake({name: 'Snake'});
    component_templates['Default mixer'] = va.Mixer({name: 'Mixer'});
    component_templates['QU-24 Mixer'] = va.Mixer({
        name: "QU-24 Mixer",
        inputChannels: 24,
        outputChannels: 12,
        image: "qu24_0",
    });
    component_templates['Default amplifier'] = va.Amplifier({name: 'Amplifier'});
    component_templates['Default speaker'] = va.Speaker({name: 'Speaker'});
}
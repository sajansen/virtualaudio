import React, {Component} from "react";
import ReactDOM from "react-dom";
import ConnectionTable from 'components/connectionTable';
import '~/style/modal.sass';

$(function(){
    const componentTemplate = require('./templates/component.html');
    $('body').append(componentTemplate);
});

export function showComponent(component) {
    console.debug(`Creating modal for component: ${component.name}`);
    document.getElementById("component-modal-title").innerText = component.name;
    document.getElementById("component-modal-type").innerText = component.type;

    if (component.inputs.length > 1 || component.outputs.length > 1) {
        ReactDOM.render(<ConnectionTable component={component}/>, document.getElementById("component-modal-ioputs"));
    }

    $("#component-modal").modal('show');
}

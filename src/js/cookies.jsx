export function setCookie(name, value, days) {
  let expires = "";
  if (days != null) {
    let date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

export function getCookie(name) {
  let nameEQ = name + "=";
  let cookiesString = document.cookie.split(';');
  for (var i = 0; i < cookiesString.length; i++) {
    let currentCookie = cookiesString[i].trimLeft();

    if (currentCookie.indexOf(nameEQ) === 0) {
      return currentCookie.substring(nameEQ.length);
    }
  }
  return null;
}

export function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=-99999999;';
}
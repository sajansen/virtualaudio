import React, {Component} from "react";
import ReactDOM from "react-dom";
import ThemeSwitchButton from 'components/themeSwitch';
import '~/style/dark-theme.sass';

// Add button to page
const themeSwitchButtonDiv = document.createElement('div');
themeSwitchButtonDiv.id = 'theme-switch-button';
document.body.append(themeSwitchButtonDiv);

// Init button
ReactDOM.render(<ThemeSwitchButton/>, document.getElementById("theme-switch-button"));

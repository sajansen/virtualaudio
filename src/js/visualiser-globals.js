import * as cnvs from './visualiser-paint.js';
import {setComponentGeometry} from "~/js/visualiser-build";

export const COMPONENT_ID_PREFIX = "component-";
export const IMAGE_COMPONENT_PATH = "img/components";
export const IMAGE_COMPONENT_POSTFIX = ".jpg";
export const IMAGE_ABSENT_COLOR = "#faa";
export const DEBUG = true;

export function NewPage() {

  this.selectedComponent = null;
  this.highlightRoute = {
    upstream: {
      components: [],
      connections: [],
    },
    downstream: {
      components: [],
      connections: [],
    }
  };

  let animationFrameID;

  this.resetHighlightRoute = function () {
    if (this.selectedComponent != null) {
      this.selectedComponent.geometry.zIndex = 0;
      setComponentGeometry(this.selectedComponent);
    }
    this.highlightRoute.upstream.components.forEach(component => component.highlighted = false);
    this.highlightRoute.upstream.connections.forEach(connection => connection.highlighted = false);
    this.highlightRoute.downstream.components.forEach(component => component.highlighted = false);
    this.highlightRoute.downstream.connections.forEach(connection => connection.highlighted = false);
    this.highlightRoute.upstream.components = [];
    this.highlightRoute.upstream.connections = [];
    this.highlightRoute.downstream.components = [];
    this.highlightRoute.downstream.connections= [];
    this.selectedComponent = null
  };


  this.animate = function () {
    cnvs.repaint();
    animationFrameID = requestAnimationFrame(this.animate.bind(this));
  };

  this.startAnimation = function () {
    this.animate();
  };

  this.stopAnimation = function () {
    cancelAnimationFrame(animationFrameID);
    cnvs.repaint();
  };
}

export const page = new NewPage();

export const visualisation = document.getElementById("visualisation");

export const canvas = document.getElementById("canvas");
export const context = canvas.getContext("2d");
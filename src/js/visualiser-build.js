import './utils.js';
import * as global from './visualiser-globals.js';
import * as tools from './visualiser-tools.js';
import * as va from './virtualaudio.js';

export function getDimensions(element) {
  let parentBounding = global.visualisation.getBoundingClientRect();
  let childBounding = element.getBoundingClientRect();

  let bounding = new DOMRect(
    childBounding.x - parentBounding.x,
    childBounding.y - parentBounding.y,
    childBounding.width,
    childBounding.height,
  );

  let centerX = bounding.x + bounding.width / 2;
  let centerY = bounding.y + bounding.height / 2;

  return {
    bounding: bounding,
    center: {
      x: centerX,
      y: centerY
    },
  }
}

/**
 *    BUILDING
 */

export function createSchemeInfo() {
  let element = document.getElementById("scheme-info");
  if (!element) {
    element = document.createElement("div");
    element.id = "scheme-info";
    document.getElementById("visualisation-wrapper").appendChild(element);
  }
  element.innerHTML = "Scheme: {name}<br/>Version: {version}".sprintf(va.scheme);
}

export function createComponentName(component) {
  let element = document.createElement("div");
  element.classList.add("component-name");
  element.innerHTML = component.name;
  return element;
}

export function createComponentElement(component) {
  let element = document.createElement("div");
  element.id = global.COMPONENT_ID_PREFIX + component.id;
  element.setAttribute("data-component-id", component.id);
  element.classList.add("component");
  setComponentGeometry(component, element);

  let componentImage = (component.image != null && component.image !== "") ? component.image : "unknown";
  element.style.backgroundImage = "url('" + global.IMAGE_COMPONENT_PATH + "/" + componentImage + global.IMAGE_COMPONENT_POSTFIX + "')";

  if (component.name) {
    element.appendChild(createComponentName(component));
  }

  return element;
}

export function createComponentsElement() {
  va.scheme.components.forEach((component) => {
    if (!component)
      return;

    let element = createComponentElement(component);
    global.visualisation.appendChild(element);
  });
}

export function rebuild() {
  tools.clearChildren(global.visualisation);
  createComponentsElement();
  buildHighlightRoute();
}

export function buildHighlightRoute() {
  $(".component").removeClass("highlight-component")
    .removeClass("highlight-component-selected")
    .removeClass("highlight-component-upstream")
    .removeClass("highlight-component-downstream")
    .css("opacity", "");

  if (!global.page.selectedComponent)
    return;

  const startElement = document.getElementById(global.COMPONENT_ID_PREFIX + global.page.selectedComponent.id);
  startElement.classList.add("highlight-component");
  startElement.classList.add("highlight-component-selected");
  startElement.style.zIndex = global.page.selectedComponent.geometry.zIndex;

  global.page.highlightRoute.upstream.components.forEach(function (component) {
    const element = document.getElementById(global.COMPONENT_ID_PREFIX + component.id);
    element.classList.add("highlight-component");
    element.classList.add("highlight-component-upstream");
  });

  global.page.highlightRoute.downstream.components.forEach(function (component) {
    const element = document.getElementById(global.COMPONENT_ID_PREFIX + component.id);
    element.classList.add("highlight-component");
    element.classList.add("highlight-component-downstream");
  });

  $(".component").css("opacity", "0.5");
  $(".highlight-component").css("opacity", "");
}

export function setComponentGeometry(component, domElement = null) {
  if (domElement == null) {
    domElement = document.getElementById(global.COMPONENT_ID_PREFIX + component.id);
  }

  domElement.style.left = component.geometry.x + "px";
  domElement.style.top = component.geometry.y + "px";
  domElement.style.zIndex = component.geometry.zIndex;
  domElement.style.width = component.geometry.width + "px";
  domElement.style.height = component.geometry.height + "px";
}

export function updateSchemeComponent(component, domElement = null) {
  if (component == null && domElement == null) {
    console.error("Cannot update component without component or DOM element.");
    return;
  }

  if (domElement == null) {
    domElement = document.getElementById(global.COMPONENT_ID_PREFIX + component.id);
  } else if (component == null) {
    const id = parseInt(domElement.dataset.componentId);
    component = va.scheme.getComponent(id);
  }

  component.geometry.x = parseInt(domElement.style.left, 10);
  component.geometry.y = parseInt(domElement.style.top, 10);
  component.geometry.zIndex = parseInt(domElement.style.zIndex, 10);
  component.geometry.width = parseInt(domElement.style.width, 10);
  component.geometry.height = parseInt(domElement.style.height, 10);
}
import * as global from './visualiser-globals.js';
import * as tools from './visualiser-tools.js';
import * as va from './virtualaudio.js';

export const debug = {};

if (global.DEBUG || window.va.debug){
    window.va = debug;

    debug.page = global.page;
    debug.scheme = va.scheme;
    debug.component_templates = tools.component_templates;
}
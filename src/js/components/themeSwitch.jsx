import React, {Component} from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import PillSwitch from 'components/PillSwitch';
import {getCookie, setCookie} from "~/js/cookies";

export default class ThemeSwitchButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDarkTheme: false,
    };

    this.cookieName = "darkTheme";
    if (getCookie(this.cookieName) === "1") {
      this.enableDarkTheme();
      this.state.isDarkTheme = true;
    }

    this.handleSwitch = this.handleSwitch.bind(this);
  }

  enableDarkTheme() {
    document.body.classList.add('dark-theme');
    setCookie(this.cookieName, "1");

    this.setState({isDarkTheme: true});
  }

  disableDarkTheme() {
    document.body.classList.remove('dark-theme');
    setCookie(this.cookieName, "0");

    this.setState({isDarkTheme: false});
  }

  handleSwitch(e) {
    const newStateIsDarkTheme = !this.state.isDarkTheme;

    // Enable dark theme
    if (newStateIsDarkTheme) {
      this.enableDarkTheme();
    } else {
      this.disableDarkTheme()
    }
  }

  render() {
    const switchStatus = this.state.isDarkTheme ? 'on' : 'off';
    return (
      <div>
        <PillSwitch switchState={this.state.isDarkTheme}
                    className={"theme-switch-button theme-switch-button-" + switchStatus}
                    onClick={this.handleSwitch}
                    title={"Click here to switch dark theme " + (this.state.isDarkTheme ? 'off' : 'on')}/>
      </div>
    );
  }
}
import React, {Component} from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import '~/style/pill-switch.sass'

/**
 * <PillSwitch
 *      switchState=true|false
 *      onClick=callbackFunction()
 *      />
 */
export default class PillSwitch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            switchState: props.switchState,
        };

        this.handleSwitch = this.handleSwitch.bind(this);
    }

    componentWillReceiveProps(props){
        this.setState({
            switchState: props.switchState,
        });
    }

    handleSwitch(e){
        this.props.onClick(e);
    }

    render () {
        return (
            <label className='pill-switch'>
                <input type='button'
                       data-switchState={this.state.switchState ? 'on' : 'off'}
                       onClick={this.handleSwitch} />
                <span className='slider round' />
            </label>
        )
    }
}
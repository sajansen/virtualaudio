import React from "react";
import '~/style/connectiontable.sass';

class InternalConnectionButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      component: props.component,
      inputChannel: props.inputChannel,
      outputChannel: props.outputChannel,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({
      component: props.component,
      inputChannel: props.inputChannel,
      outputChannel: props.outputChannel,
    });
  }

  handleInputChange() {
    const wasConnected = this.state.component.isConnected(this.state.inputChannel).to(this.state.outputChannel);

    if (wasConnected) {
      // Remove internal connection
      this.state.component.disconnect(this.state.inputChannel).from(this.state.outputChannel);
    } else {
      // Add internal connection
      this.state.component.connect(this.state.inputChannel).to(this.state.outputChannel);
    }

    this.forceUpdate();
  }

  render() {
    const isConnected = this.state.component.isConnected(this.state.inputChannel).to(this.state.outputChannel);
    const title = 'Click to ' + (isConnected ? 'disconnect' : 'connect') + '!';
    const icon = isConnected ? <i className="fas fa-minus"/> : <i className="fas fa-plus"/>;
    return (
      <button
        className="internalConnectionButton"
        name={this.state.component.name}
        onClick={this.handleInputChange}
        data-isConnected={isConnected}
        title={title}>
        {icon}
      </button>
    );
  }
}

export default class ConnectionTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      component: props.component,
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      component: props.component,
    })
  }

  render() {
    // Sort
    const sort = (a, b) => {
      if (isNaN(a.name) || isNaN(b.name))
        return a.name > b.name;
      else
        return parseInt(a.name) > parseInt(b.name);
    };
    const inputs = this.state.component.inputs.sort(sort);
    const outputs = this.state.component.outputs.sort(sort);

    // Prepare
    const inputsHeaderRow =
      <tr>
        <th></th>
        {inputs.map((ioput) =>
          <th>
            {ioput.name}
            <small>{ioput.type}</small>
          </th>)}
        <th></th>
      </tr>;

    let rows = [];

    rows.push(inputsHeaderRow);
    outputs.forEach((output) => {
      const outputsHeaderRow =
        <th>
          {output.name}
          <small>{output.type}</small>
        </th>;

      let row = [];
      row.push(outputsHeaderRow);

      inputs.forEach((input) => {
        row.push(
          <td>
            <InternalConnectionButton
              component={this.state.component}
              inputChannel={input.name}
              outputChannel={output.name}/>
          </td>
        );
      });

      if (outputs.length > 5 && inputs.length > 5)
        row.push(outputsHeaderRow);

      rows.push(<tr>{row}</tr>);
    });
    if (outputs.length > 5 && inputs.length > 5)
      rows.push(inputsHeaderRow);

    return (<table className="connectiontable-table">{rows}</table>);
  }
};
import * as global from './visualiser-globals.js';
import * as va from './virtualaudio.js';
import {ConnectionType} from "./virtualaudio";

/**
 *   PAINTING
 */

export function getDimensions(element) {
  let parentBounding = global.visualisation.getBoundingClientRect();
  let childBounding = element.getBoundingClientRect();

  let bounding = new DOMRect(
    childBounding.x - parentBounding.x,
    childBounding.y - parentBounding.y,
    childBounding.width,
    childBounding.height,
  );

  let centerX = bounding.x + bounding.width / 2;
  let centerY = bounding.y + bounding.height / 2;

  return {
    bounding: bounding,
    center: {
      x: centerX,
      y: centerY
    },
  }
}

export function getConnectionDotPosition(connection, invert = false) {
  const offset = 3;   // The distance the dot must be placed from the outside of the component

  const connectionStart = document.getElementById(global.COMPONENT_ID_PREFIX + connection.output.component.id);
  const connectionStop = document.getElementById(global.COMPONENT_ID_PREFIX + connection.input.component.id);

  const dimensionsStart = getDimensions(connectionStart);
  const dimensionsStop = getDimensions(connectionStop);

  const deltaY = dimensionsStop.center.y - dimensionsStart.center.y;
  const deltaX = dimensionsStop.center.x - dimensionsStart.center.x;

  // Get angle
  const gamma = (deltaY === 0) ? 0.5 * Math.PI    // Default value, because we can't divide by 0
                               : Math.abs(Math.atan(deltaX / deltaY));

  // Calculate x coordinate
  const maxX = dimensionsStart.bounding.width / 2 + offset;
  const maxY = dimensionsStart.bounding.height / 2 + offset;

  let dotX = Math.min(maxX * Math.tan(gamma), maxX);
  if (deltaX < 0)
    dotX = -1 * dotX;

  // Calculate y coordinate
  let dotY = (gamma === 0) ? maxY   // Default value if gamma == 0, because we can't divide by 0
                           : Math.min(maxY / Math.tan(gamma), maxY);
  if (deltaY < 0)
    dotY = -1 * dotY;

  if (invert) {
    return {
      x: dimensionsStart.center.x + dotX,
      y: dimensionsStart.center.y + dotY,
    }
  } else {
    return {
      x: dimensionsStop.center.x - dotX,
      y: dimensionsStop.center.y - dotY,
    }
  }
}

export function paintConnections() {
  const connectionLines = getConnectionLines();
  realignConnectionLines(connectionLines);

  connectionLines.forEach(function (connectionLine) {
    connectionLine.draw(global.context);
  });
}

export function clearCanvas() {
  global.context.clearRect(0, 0, global.canvas.getBoundingClientRect().width, global.canvas.getBoundingClientRect().height);
}

export function repaint() {
  clearCanvas();
  paintConnections();
  paintHighlightRoute();
}

export function paintHighlightRoute() {
  paintConnections();
}

function getConnectionLines() {
  const connectionLines = [];
  va.scheme.connections.forEach(function (connection) {
    const connectionLine = new ConnectionLine(connection);
    connectionLines.push(connectionLine)
  });
  return connectionLines;
}

function realignConnectionLines(connectionLines) {
  function isSameComponentsConnection(connectionA, connectionB) {
    return connectionA.output.component === connectionB.output.component
           && connectionA.input.component === connectionB.input.component;
  }

  const beginStack = [...connectionLines];
  const sameConnectionsStack = [];

  for (let i = 0; i < beginStack.length; i++) {
    const sameConnections = [beginStack[i]];
    for (let j = i + 1; j < beginStack.length; j++) {
      if (isSameComponentsConnection(beginStack[i].connection, beginStack[j].connection)) {
        sameConnections.push(beginStack[j]);
        beginStack.splice(j--, 1);
      }
    }
    sameConnectionsStack.push(sameConnections);
    beginStack.splice(i--, 1);
  }

  sameConnectionsStack.forEach(sameConnections => {
    if (sameConnections.length <= 1) {
      return;
    }

    const spacing = 1.3; // 1 px spacing
    let newPointX = -1 * sameConnections.length / 2 * spacing;
    sameConnections.forEach(connectionLine => {
      connectionLine.dimensionsStartDot.x -= newPointX;
      connectionLine.dimensionsStopDot.x -= newPointX;
      newPointX += spacing;
    });
  });
}

class ConnectionLine {
  constructor(connection) {
    this.connection = connection;

    this.dimensionsStartDot = getConnectionDotPosition(connection, false);
    this.dimensionsStopDot = getConnectionDotPosition(connection, true);
    this.lineWidth = connection.highlighted ? 3 : 1;
  }

  draw(context) {
    // Determine color
    switch (this.connection.type) {
      case ConnectionType.WIRELESS:
        context.strokeStyle = "#007dff";
        break;
      case ConnectionType.CABLE:
      default:
        context.strokeStyle = "#000000";
    }

    context.beginPath();
    context.lineWidth = this.lineWidth;
    context.lineCap = "round";
    context.moveTo(this.dimensionsStartDot.x, this.dimensionsStartDot.y);
    context.lineTo(this.dimensionsStopDot.x, this.dimensionsStopDot.y);
    context.stroke();

    new ConnectionDot(this.dimensionsStartDot.x, this.dimensionsStartDot.y, true).draw(context);
    new ConnectionDot(this.dimensionsStopDot.x, this.dimensionsStopDot.y, false).draw(context);
  }
}

class ConnectionDot {
  constructor(x, y, isInput) {
    this.x = x;
    this.y = y;
    this.dotSize = 3;
    this.isInput = isInput;
    return this;
  }

  draw(context) {
    // Calculate position of connection dot
    context.beginPath();
    context.arc(this.x, this.y, this.dotSize, 0, 2 * Math.PI);
    // Fill it up
    context.fillStyle = this.isInput ? 'black' : 'white';
    context.fill();
    // Draw outline
    context.lineWidth = 1;
    context.stroke();
  }
}
export function Scheme() {
  this.name = "My Project";
  this.version = "1.0.1";
  this.components = [];
  this.connections = [];
  this.highestComponentID = 0;

  this.addComponent = function (component) {
    console.debug(`Adding ${component.name} to scheme ${this.name}`);
    if (component.id == null) {
      component.id = this.getNewComponentID();
    } else if (component.id > this.highestComponentID) {
      this.highestComponentID = component.id;
    }

    if (this.getComponent(component.id)) {
      throw "Component " + component.name + " already exists in sheme's components!";
    }
    this.components.push(component);
    return component;
  };

  this.getNewComponentID = function () {
    return ++this.highestComponentID;
  };

  this.getComponent = function (id) {
    return this.components.find(function (c) {
      return c.id === id;
    });
  };

  this.deleteComponent = function (id) {
    console.debug(`Deleting component with ID: ${id}.`);
    this.components = this.components.filter(c => c.id !== id);
    this.connections = this.connections.filter(c => c.output.component.id !== id && c.input.component.id !== id);
  };

  this.findConnectionWithOutput = function (component, channel) {
    return this.connections.find(function (c) {
      return c.output.component.id === component.id && c.output.channel.toString() === channel;
    });
  };

  this.findConnectionWithInput = function (component, channel) {
    return this.connections.find(function (c) {
      return c.input.component.id === component.id && c.input.channel.toString() === channel;
    });
  };

  this.reset = function () {
    this.name = "";
    this.version = "1.0.0";
    this.components = [];
    this.connections = [];
    this.highestComponentID = 0;
  }
}

export const scheme = new Scheme();

/**************************************************************************/

export const IOputType = Object.freeze({
                                         XLR: 1,
                                         JACK: 2,
                                         MINIJACK: 3,
                                         SPEAKON: 4,
                                         TULP: 5,
                                         WIRELESS: 6,
                                       });

export function Geometry(x = 0, y = 0) {
  this.x = x;
  this.y = y;
  this.width = 100;
  this.height = 100;
  this.zIndex = 0;

  this.containsPoint = function (x, y) {
    return x > this.x && x < this.x + this.width
           && y > this.y && x < this.y + this.height;
  };

  this.placeRightOf = function (component, margin = 40) {
    this.x = component.geometry.x + component.geometry.width + margin;
    this.y = component.geometry.y;
    return this;
  };

  this.placeLeftOf = function (component, margin = 40) {
    this.x = component.geometry.x - margin - this.width;
    this.y = component.geometry.y;
    return this;
  };

  this.placeUnderneathOf = function (component, margin = 40) {
    this.x = component.geometry.x;
    this.y = component.geometry.y + component.geometry.height + margin;
    return this;
  };

  this.moveAmountTimesLengthRight = function (amount, margin = 40) {
    this.x = this.x + amount * (margin + this.width);
    return this;
  };

  this.moveAmountTimesLengthUp = function (amount, margin = 40) {
    this.y = this.y - amount * (margin + this.height);
    return this;
  };

  this.moveHalfLengthLeft = function (margin = 40) {
    return this.moveAmountTimesLengthRight(-0.5, margin);
  };

  this.moveOneLengthLeft = function (margin = 40) {
    return this.moveAmountTimesLengthRight(-1, margin);
  };

  this.moveHalfLengthRight = function (margin = 40) {
    return this.moveAmountTimesLengthRight(0.5, margin);
  };

  this.moveOneLengthRight = function (margin = 40) {
    return this.moveAmountTimesLengthRight(1, margin);
  };

  this.moveHalfLengthUp = function (margin = 40) {
    return this.moveAmountTimesLengthUp(0.5, margin);
  }

  this.moveHalfLengthDown = function (margin = 40) {
    return this.moveAmountTimesLengthUp(-0.5, margin);
  }
}

export function Component({
                            name = null,
                            type = null,
                            group = null,
                            category = null,
                            image = "unknown",
                            inputs = [],
                            outputs = [],
                          }) {
  this.id = null;
  this.name = name;
  this.type = type;
  this.group = group;
  this.category = category;
  this.image = image;
  this.inputs = inputs;
  this.outputs = outputs;
  this.internalConnections = [];
  this.geometry = new Geometry();
  this.highlighted = false;

  this.addToScheme = function (scheme) {
    this.id = scheme.getNewComponentID();
    return scheme.addComponent(this);
  };

  this.findInternalConnectionsWithInput = function (channel) {
    return this.internalConnections.filter(function (c) {
      return c.inputChannel.toString() === channel;
    });
  };

  this.findInternalConnectionsWithOutput = function (channel) {
    return this.internalConnections.filter(function (c) {
      return c.outputChannel.toString() === channel;
    });
  };

  this.connect = function (inputChannel) {
    let _this = this;

    function _to(outputChannels) {
      if (!Array.isArray(outputChannels)) {
        outputChannels = [outputChannels];
      }

      outputChannels.forEach(function (outputChannel) {
        console.debug(`Connecting ${inputChannel} to ${outputChannel} for ${_this.name}.`);
        _this.internalConnections.push({
                                         inputChannel: inputChannel,
                                         outputChannel: outputChannel
                                       });
      })
    }

    return {
      to: _to
    }
  };

  this.disconnect = function (inputChannel) {
    let _this = this;

    function _from(outputChannel) {
      console.debug(`Disconnecting ${inputChannel} from ${outputChannel} for ${_this.name}.`);
      _this.internalConnections = _this.internalConnections.filter((c) => !(c.inputChannel === inputChannel && c.outputChannel === outputChannel));
    }

    return {
      from: _from
    }
  };

  this.isConnected = function (inputChannel) {
    let _this = this;

    function _to(outputChannel) {
      return Boolean(_this.internalConnections.find((c) => c.inputChannel === inputChannel && c.outputChannel === outputChannel));
    }

    return {
      to: _to
    }
  };
}

export function IOput({name = "0", type = IOputType.XLR}) {
  this.name = name;
  this.type = type;

  this.setName = function (name) {
    this.name = name;
    return this;
  };

  this.setType = function (type) {
    this.type = type;
    return this;
  }
}

export const ConnectionType = Object.freeze({
                                              CABLE: 1,
                                              WIRELESS: 2,
                                            });

export function connect(outputComponent, outputChannel = "0") {
  function _with(type) {
    function _to(inputComponent, inputChannel = "0") {
      scheme.connections.push({
                                output: {
                                  component: outputComponent,
                                  channel: outputChannel,
                                },
                                input: {
                                  component: inputComponent,
                                  channel: inputChannel,
                                },
                                type: type,
                                highlighted: false
                              });
    }

    return {
      to: _to
    }
  }

  return {
    with: _with,
    to: _with(ConnectionType.CABLE).to
  }
}


/** Default components **/

export function Microphone({name}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "microphone",
                                    category: "microphones",
                                    image: "sm58_0",
                                    outputs: [new IOput({name: "0", type: IOputType.XLR})],
                                  });
  return scheme ? component.addToScheme(scheme) : component;
}

export function WirelessReceiver({name}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "receiver",
                                    category: "wireless",
                                    image: "Sennheiser_EW_300_0",
                                    inputs: [new IOput({type: IOputType.WIRELESS})],
                                    outputs: [
                                      new IOput({name: "line", type: IOputType.JACK}),
                                      new IOput({name: "mic", type: IOputType.XLR})
                                    ],
                                  });
  component.connect(component.inputs[0].name.toString()).to("line");
  component.connect(component.inputs[0].name.toString()).to("mic");
  return scheme ? component.addToScheme(scheme) : component;
}

export function DI({name, channels = 2}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "di",
                                    category: "others",
                                    image: "di20_0",
                                  });

  // Add in/outputs
  for (let i = 1; i <= channels; i++) {
    component.inputs.push(new IOput({name: i.toString(), type: IOputType.JACK}));
    component.outputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));

    component.connect(i.toString()).to(i.toString());
  }
  return scheme ? component.addToScheme(scheme) : component;
}

export function Snake({name, channels = 24}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "stageblock",
                                    category: "others",
                                    image: "dap-snake-24ch_0",
                                  });

  // Add inputs/outputs
  for (let i = 1; i <= channels; i++) {
    component.inputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));
    component.outputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));

    component.connect(i.toString()).to(i.toString());
  }
  return scheme ? component.addToScheme(scheme) : component;
}

export function Mixer({name, inputChannels = 0, outputChannels = null, image = "soundcraftgb2_0"}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "mixer",
                                    category: "mixers",
                                    image: image,
                                  });

  // Add inputs
  for (let i = 1; i <= inputChannels; i++) {
    component.inputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));
  }

  // Add outputs
  if (outputChannels === null) {
    component.outputs = [
      new IOput({name: "mainL", type: IOputType.XLR}),
      new IOput({name: "mainR", type: IOputType.XLR})
    ];
  } else {
    for (let i = 1; i <= outputChannels; i++) {
      component.outputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));
    }
  }
  return scheme ? component.addToScheme(scheme) : component;
}

export function Amplifier({name, channels = 2}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "amplifier",
                                    category: "amplifiers",
                                    image: "crown_xls_drivecore_1000_0",
                                  });

  // Add in/outputs
  for (let i = 1; i <= channels; i++) {
    component.inputs.push(new IOput({name: i.toString(), type: IOputType.XLR}));
    component.outputs.push(new IOput({name: i.toString(), type: IOputType.SPEAKON}));

    component.connect(i.toString()).to(i.toString());
  }
  return scheme ? component.addToScheme(scheme) : component;
}

export function Speaker({name}, scheme = null) {
  const component = new Component({
                                    name: name,
                                    type: "speaker",
                                    category: "speakers",
                                    image: "dynacord-d8",
                                    inputs: [new IOput({name: "0", type: IOputType.SPEAKON})],
                                  });
  return scheme ? component.addToScheme(scheme) : component;
}

